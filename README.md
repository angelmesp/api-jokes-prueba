
## Prueba Angel Montesflores

Para instalar debe seguir los siguientes pasos:

- Instalar las dependencias de composer:
> composer install

- Instalar las dependencias de Nodejs:
> npm install

-Levantar el servidor con artisan:
> php artisan serve


# Url de la API GET
> http://127.0.0.1:8000/api/jokes