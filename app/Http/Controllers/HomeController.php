<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    
    
    function getJokes($url, $cantidad_peticiones) {
        $urls = [];
        for ($i=0; $i < $cantidad_peticiones; $i++) {
            array_push($urls, $url);
        }
        $curly = array();
        $resultado = array();
        $pm = curl_multi_init();
        foreach ($urls as $id => $d) {
            $curly[$id] = curl_init();
            if(is_array($d) && !empty($d['url'])) {
                $url = $d['url'];
            } else {
                $url = $d;
            }
            curl_setopt($curly[$id], CURLOPT_URL, $url);
            curl_setopt($curly[$id], CURLOPT_HEADER, 0);
            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
            
            curl_multi_add_handle($pm, $curly[$id]);
        }
        $ejecutando = null;
        do {
            curl_multi_exec($pm, $ejecutando);
        } while($ejecutando > 0);
        foreach($curly as $id => $c) {
            $resultado[$id] = curl_multi_getcontent($c);
            curl_multi_remove_handle($pm, $c);
        }
        curl_multi_close($pm);
        return $resultado;
    }
    
    private function existeJoke($newJoke, $joke){
        $response = false;
        foreach ($newJoke as $key => $jokeTemp) {
            $response = $jokeTemp["id"] === $joke->id ? true : false;
        }
        return $response;
        
    }
    private function addJokes($jokesResponse){
        $newJoke = [];
        foreach ($jokesResponse as $key => $joke) {
            
            $joke = json_decode($joke);
            $addJokes = [
                "icon_url" => $joke->icon_url,
                "id" => $joke->id,
                "value" => $joke->value
            ];
            $existe = $this->existeJoke($newJoke, $joke) === false ? array_push($newJoke, $addJokes) : true;
        }
        return $newJoke;
    }
    public function index()
    {
        $url_peticion = "https://api.chucknorris.io/jokes/random";
        $jokes = [];
        $cantidad_peticiones = 15;
        $jokesResponse = $this->getJokes($url_peticion, $cantidad_peticiones);
        $cantidad = 0;
        $jokes = $this->addJokes($jokesResponse);
        
        while(count($jokes) < $cantidad_peticiones){
            $cantidad_nueva_peticion = $cantidad_peticiones - count($jokes);
            $jokesResponse = $this->getJokes($url_peticion, $cantidad_peticiones);
            $jokesTemp = $this->addJokes($jokesResponse);
            foreach ($jokesTemp as $jokeTemp) {
                array_push($jokes, $jokeTemp);
            }
        }
        
        return response()->json($jokes);
    }
}
